from distutils.core import setup

# setup(name='Game of life',
#       version='1.0',
#       py_modules=['gameoflife'],
#       package_dir={'mypkg': 'src/gameoflife'},
#       )



setup(name = "gameoflife",
      version = "1.0",
      description = "Game of life",
      author = "Milan Cingros",
      author_email = "milan@cingros.cz",
      url = "http://milan.cingros.cz",
      #Name the folder where your packages live:
      #(If you have other packages (dirs) or modules (py files) then
      #put them into the package directory - they will be found
      #recursively.)
      packages = ['gameoflife'],
      #'package' package must contain files (see list above)
      #I called the package 'package' thus cleverly confusing the whole issue...
      #This dict maps the package name =to=> directories
      #It says, package *needs* these files.
      # package_data = {'package' : files },
      #'runner' is in the root.
      scripts = ["game-of-life"],
      long_description = """RGame of life"""
      #
      #This next part it for the Cheese Shop, look a little down the page.
      #classifiers = []
      )