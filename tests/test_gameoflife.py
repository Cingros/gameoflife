from gameoflife.gameoflife import result, cell, next_iter, main, check_input_data
import io
import pytest

# python -m pytest --capture=sys --cov-branch --cov gameoflife

def test_result():
    assert result(0, 0) == 0
    assert result(1, 0) == 0
    assert result(0, 4) == 0
    assert result(1, 4) == 0
    assert result(0, 3) == 1
    assert result(1, 3) == 1
    assert result(0, 2) == 0
    assert result(1, 2) == 1

def test_cell():
    grid = ['0000',
            '0110',
            '0110',
            '0000']
    assert cell(grid, 0, 0, len(grid), len(grid[0])) == 1
    assert cell(grid, 1, 1, len(grid), len(grid[0])) == 3
    assert cell(grid, 0, 2, len(grid), len(grid[0])) == 2

def test_next_iter(capsys):
    grid = ['0000',
            '0110',
            '0110',
            '0000']

    next_iter(grid, 4, 4)
    out, err = capsys.readouterr()
    assert out.split() == grid

def test_next_iter2(monkeypatch, capsys):
    grid1 = ['00000',
             '00100',
             '00100',
             '00100',
             '00000']

    grid2 = ['00000',
             '00000',
             '01110',
             '00000',
             '00000']


    monkeypatch.setattr('sys.stdin', io.StringIO('\n'.join(grid1)))
    main()
    out, err = capsys.readouterr()
    assert out.split() == grid2

def test_check_input_data():
    grid1 = ['01',
             '10']
    assert check_input_data(grid1, 2) == None

def test_check_input_data2():
    grid1 = ['011',
             '10']
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        check_input_data(grid1, 2)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1

def test_check_input_data3():
    grid1 = ['02',
             '10']
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        check_input_data(grid1, 2)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1
