import sys

def check_input_data(data, columns):
    """
    simple check of input data
    :param data: input grid
    :param columns: number of collumns of first line
    :return: returns if input data are valid, exits on error
    """
    for line in data:
        sline = line.rstrip()
        if len(sline) != columns:
            print("Error line: {}".format(sline))
            exit(1)

        for char in sline:
            if char not in ('0', '1'):
                print("Error char '{}' in line: '{}'".format(char, sline))
                exit(1)

def next_iter(data, lines, columns):
    """
    :param data: input grid
    :param lines: number of lines
    :param columns: number of columns
    :return: new grid
    """
    for i in range(0, lines):
        for j in range(0, columns):
            live = cell(data, i, j, lines, columns)
            res = result(data[i][j], live)
            print(res, end='')
        print()

def coords(line, column, lines, columns):
    """
    yields coordinates of 8 neighbors
    :param line: current line
    :param column: curent column
    :param lines: number of lines
    :param columns: number of collumns
    """
    for x in range(-1,2):
        for y in range(-1,2):
            if x == 0 and y == 0: continue
            i = (line + x) % lines
            j = (column + y) % columns
            yield (i, j)

def cell(data, line, column, lines, columns):
    """
    :param data: input grid
    :param line: current line
    :param column: curent column
    :param lines: number of lines
    :param columns: number of collumns
    :return: number of living neighbours
    """
    live = 0
    for (i, j) in coords(line, column, lines, columns):
        live += int(data[i][j])
    return live

def result(current, live):
    """
    :param current: current state of cell
    :param live: number fo living neighbours
    :return: new state of cell
    """
    if live < 2:
        return 0
    elif live > 3:
        return 0
    elif live == 3:
        return 1
    else:
        return current

def main():
    data = sys.stdin.readlines()
    lines = len(data)
    columns = len(data[0].rstrip())
    check_input_data(data, columns)
    next_iter(data, lines, columns)

